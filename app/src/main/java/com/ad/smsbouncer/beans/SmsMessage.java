package com.ad.smsbouncer.beans;

public class SmsMessage {
    private String senderNumber;
    private String message;

    public String getSenderNumber() {
        return senderNumber;
    }

    public void setSenderNumber(String senderNumber) {
        this.senderNumber = senderNumber;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SmsMessage(String senderNumber, String message){
        this.senderNumber = senderNumber;
        this.message = message;
    }
}
