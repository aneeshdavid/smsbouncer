package com.ad.smsbouncer.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;
import java.util.List;

public class DBHelper extends SQLiteOpenHelper {

    private static DBHelper instance = null;
    public static final String DATABASE_NAME = "SmsBouncer.db";
    public static final String Table_NUMBERS = "mNumbers";
    public static final String Table_APP_CONFIG = "appConfig";

    private DBHelper(Context context) {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS mNumbers (id integer primary key AUTOINCREMENT, number text)");
        db.execSQL("CREATE TABLE IF NOT EXISTS appConfig (id integer primary key AUTOINCREMENT, name text,value text)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
       // db.execSQL("DROP TABLE IF EXISTS mNumbers");
        onCreate(db);
    }

    public boolean addNumbers(List<String> mNumbers){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = null;
        for(String number : mNumbers){
            contentValues = new ContentValues();
            contentValues.put("number", number);
            db.insert(Table_NUMBERS, null, contentValues);
        }
        return true;
    }

    public boolean removeNumbers(List<String> mNumbers){
        for(String number : mNumbers){
            Cursor cur = getNumber(number);
            if(cur != null){
                removeNumbers(number);
            }
        }
        return true;
    }

    public Cursor getNumber(String number) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from mNumbers where number="+number+"", null );
        return res;
    }


    private Integer removeNumbers(String mNumber){

        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(Table_NUMBERS,"number = ?", new String[] { mNumber});
    }

    public ArrayList<String> getAllNumbers() {
        ArrayList<String> array_list = new ArrayList<String>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from " +Table_NUMBERS, null );
        res.moveToFirst();
        while(res.isAfterLast() == false){
            array_list.add(res.getString(res.getColumnIndex("number")));
            res.moveToNext();
        }
        return array_list;
    }

    public static DBHelper getInstance(Context context)
    {
        if (instance == null) {
            instance = new DBHelper(context);
        }

        return instance;
    }
}
