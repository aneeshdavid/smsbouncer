package com.ad.smsbouncer.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.ad.smsbouncer.beans.SmsMessage;
import com.ad.smsbouncer.util.InternetCheck;
import com.ad.smsbouncer.util.MessageQueue;

public class InternetStatusChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        MessageQueue.push();
    }
}
