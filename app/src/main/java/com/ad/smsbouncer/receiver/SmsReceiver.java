package com.ad.smsbouncer.receiver;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.ad.smsbouncer.util.MessageQueue;

import java.util.Base64;
import java.util.List;

public class SmsReceiver extends BroadcastReceiver {

    final static String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    final static String TAG = "SmsReceiver";
    final static String SenderNum = "senderNum:";
    final static String message = "message:";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(SMS_RECEIVED)) {

            final Bundle bundle = intent.getExtras();

            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);

                    String senderNum = currentMessage.getDisplayOriginatingAddress();
                    String message = currentMessage.getDisplayMessageBody();

                    Log.i(TAG, String.format("%s, %s, %s, %s", SenderNum, senderNum, message, message));

                    MessageQueue.add(new com.ad.smsbouncer.beans.SmsMessage(senderNum, message));

                }

                MessageQueue.push();
            }

        }

    }
}
