package com.ad.smsbouncer.util;

import android.os.AsyncTask;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public class InternetCheck extends AsyncTask<Void,Void,Boolean> {

    private Consumer mConsumer;
    public  interface Consumer {
        void accept(Boolean internet);
    }

    public  InternetCheck(Consumer consumer) {
        mConsumer = consumer;
        execute();
    }

    @Override protected Boolean doInBackground(Void... voids) {
        try {

            Socket googleSock = new Socket();
            googleSock.connect(new InetSocketAddress("8.8.8.8", 53), 1500);
            googleSock.close();

            Socket aneeshdavidSock = new Socket();
            aneeshdavidSock.connect(new InetSocketAddress("195.201.179.80", 8124), 1500);
            aneeshdavidSock.close();

            return true;
        } catch (IOException e) {
            return false;
        }
    }

    @Override protected void onPostExecute(Boolean internet) {
        mConsumer.accept(internet);
    }
}