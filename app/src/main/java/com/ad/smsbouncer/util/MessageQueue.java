package com.ad.smsbouncer.util;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.telephony.AvailableNetworkInfo;
import android.util.Log;

import com.ad.smsbouncer.beans.SmsMessage;
import com.ad.smsbouncer.mail.GMailSender;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.function.Consumer;

import javax.activation.DataHandler;
import javax.mail.Message;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MessageQueue {

    static boolean  isConnected = false;
    static String TAG = "PUSH MESSAGE";
    private static final Queue<SmsMessage> messageQueue = new LinkedList<>();

    public static void sendMail(SmsMessage message) throws Exception{
            String messageTxt = String.format("Sender: %s %sMessage: %s", message.getSenderNumber(), "\n" , message.getMessage());
            GMailSender sender = new GMailSender("smsbouncer@gmail.com", "$welcome1");
            sender.sendMail("You have a text message - SmsBouncer",
                    messageTxt,
                    "smsbouncer@gmail.com",
                    "aneeshd60@gmail.com");
    }

    public static void pushToServer(SmsMessage message) throws Exception{
        String messageTxt = String.format("Sender: %s %sMessage: %s", message.getSenderNumber(), "\n" , message.getMessage());
        new AsyncTask<Void, Void, Void>() {
            @Override public Void doInBackground(Void... arg) {
                try {
                    HttpClient httpClient=new DefaultHttpClient();
                    HttpPost httpPost=new HttpPost(
                            "http://aneeshdavid.com:8124/pushEmail");
                    JSONObject js = new JSONObject();
                    js.put("message", messageTxt);
                    StringEntity entity = new StringEntity(js.toString());
                    entity.setContentType("application/json");
                    httpPost.setEntity(entity);
                    httpPost.setHeader("api-secret-key", "726ddd1527d679c1d8500ad3a612bc902e0b62e5");
                    httpClient.execute(httpPost);

                } catch (Exception e) {
                    Log.i("PushToServer",e.getMessage());
                }
                return null;}
        }.execute();
    }

    public static void add(SmsMessage message){
        messageQueue.add(message);
    }

    public static void push(){
        new InternetCheck(internet -> {
            boolean isConnected = internet.booleanValue();
            if(isConnected) {
                List<SmsMessage> successMessages= new LinkedList<>();
                for (SmsMessage message : messageQueue) {
                    try {
                        //sendMail(message);
                        pushToServer(message);
                        successMessages.add(message);
                    } catch (Exception e) {
                        Log.e(TAG, e.getMessage(), e);
                    }
                }
                messageQueue.removeAll(successMessages);
            }

        });


    }


}
